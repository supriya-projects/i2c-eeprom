#ifndef UART_H_
#define UART_H_

#include <stdint.h>

void uart_module_init(void);
void uart_write_data(uint8_t* data,uint16_t len);
void uart_run(void);

#endif /* UART_H_ */

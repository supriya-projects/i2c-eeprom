#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "uart.h"
#include "led.h"
#include "dbgprintf.h"
#include <usart.h>
#include "eeprom.h"

typedef enum __state_ {
	ST_START_READ,
	ST_WAIT_FOR_NEW_DATA,
	ST_PROCESS_DATA,
}state_t;

#define UART_TRANSMIT_TIMEOUT_MS 1000 //milliseconds

static state_t _state;
static bool _initialised;
static uint8_t _data_from_uart;
static bool _data_available;

static void _process_data(uint8_t data_byte);

///////////////////public functions/////////////

void uart_module_init(void)
{
	if(_initialised == true)
		return;

	_state = ST_START_READ;
	_data_available = false;
	_initialised = true;
}
void uart_write_data(uint8_t* data,uint16_t len)
{
	if(!data)
		return;

	HAL_UART_Transmit(&huart1, data, len, UART_TRANSMIT_TIMEOUT_MS);
}

void uart_run(void)
{
	HAL_StatusTypeDef result;

	if(_initialised == false)
		return;

	switch(_state) {

	case ST_START_READ:
		_data_available = false;
		result = HAL_UART_Receive_IT(&huart1, &_data_from_uart, 1);
		if(result == HAL_OK)
			_state = ST_WAIT_FOR_NEW_DATA;
		break;

	case ST_WAIT_FOR_NEW_DATA:
		if(_data_available == true)
			_state = ST_PROCESS_DATA;
		break;

	case ST_PROCESS_DATA:
		_process_data(_data_from_uart);
		_state = ST_START_READ;
		break;

	default:
		break;
	}
}

////////////////////private functions/////////////////////////

static void _process_data(uint8_t data_byte)
{
	if((data_byte >= 'A' && data_byte <= 'Z') || (data_byte >= 'a' && data_byte <= 'z')){
		if (eeprom_write(data_byte) == true)
			dbgprintf(" wrote letter %c\r\n",data_byte);
		else
			dbgprintf(" failed to write letter %c\r\n", data_byte);
	}
	else if(data_byte == '/'){
		eeprom_dump_data_on_uart();
	}
	else{
		dbgprintf("unsupported command %c [%02X]\r\n",data_byte,data_byte);
		return;
	}

//	switch (data_byte) {
//
//	case 'A':
//		dbgprintf("Received A\r\n");
//		break;
//
//	case 'B':
//		dbgprintf("Received B\r\n");
//		break;
//
//	case 'g':
//		dbgprintf("Received g\r\n");
//		led_toggle(DEV_LED_GREEN);
//		break;
//
//	case '/' :
//		eeprom_dump_data_on_uart();
//		break;
//
//	default:
//		dbgprintf("unsupported command %c [%02X]\r\n",data_byte,data_byte);
//		break;
//	}
}

/////////////////////////hal function/////////

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *uart)
{
	if(uart == &huart1)
		_data_available = true;
}

int __io_putstr(char *data,int len)
{
	uart_write_data((uint8_t*)data, len);
	return len;
}

#ifndef EEPROM_H_
#define EEPROM_H_

#include <stdbool.h>

void eeprom_module_init(void);
void eeprom_dump_data_on_uart(void);
bool eeprom_write(uint8_t data_byte);

#endif /* EEPROM_H_ */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "app.h"
#include "main.h"
#include "gpio.h"
#include "led.h"
#include "timer.h"
#include "pin.h"
#include "uart.h"
#include <usart.h>
#include "dbgprintf.h"
#include "eeprom.h"

//////////////////////////////////public functions///////////////////

void app_start(void)
{
	//app init
	uart_module_init();
	led_on(DEV_LED_GREEN);
	led_on(DEV_LED_BLUE);
	led_on(DEV_LED_RED);

	printf("hello world! printf() is cool\r\n");
	dbgprintf("This is dbgprintf\r\n");
	eeprom_module_init();

	//app super loop
	while(true) {
		uart_run();
		pin_run();
	}

}

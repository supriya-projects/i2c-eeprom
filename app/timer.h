#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct __non_blocking_delay_ {
	uint32_t delay_ms;
	uint32_t end_ts;
}non_blocking_delay_t;

void timer_delay_ms(uint32_t delay_ms);
void timer_non_blocking_delay_start(non_blocking_delay_t *delay);
bool timer_is_non_blocking_delay_complete(non_blocking_delay_t *delay);

#endif /* TIMER_H_ */

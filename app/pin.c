#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <gpio.h>
#include "pin.h"
#include "led.h"
#include "eeprom.h"

#define BUTTON_DEBOUNCE_THRESHOLD_MS 200

static volatile uint32_t button_pressed_timestamp;
static bool button_press;

////////////////////////////////public functions/////////////////////////////

void pin_set(void* gpio, uint16_t pin)
{
	if(!gpio)
		return;

	HAL_GPIO_WritePin(gpio, pin, GPIO_PIN_SET);
}

void pin_clear(void* gpio, uint16_t pin)
{
	if(!gpio)
		return;

	HAL_GPIO_WritePin(gpio, pin, GPIO_PIN_RESET);
}

void pin_toggle(void* gpio, uint16_t pin)
{

	if(!gpio)
		return;

	HAL_GPIO_TogglePin(gpio, pin);
}

void pin_run(void)
{
	if(button_press==true){
		eeprom_dump_data_on_uart();
		button_press = false;
	}
}
////////////////////////////////hal function/////////////////////////////////

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == GPIO_PIN_13) {

		if((HAL_GetTick() - button_pressed_timestamp)>BUTTON_DEBOUNCE_THRESHOLD_MS){
			button_pressed_timestamp = HAL_GetTick();
			led_toggle(DEV_LED_GREEN);
			button_press = true;
		}
	}
}

/////////////////////////////////////private functions/////////

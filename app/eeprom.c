#include <stdio.h>
#include <stdint.h>
#include "eeprom.h"
#include "dbgprintf.h"
#include <ctype.h>
#include "i2c.h"
#include <stdbool.h>
#include "pin.h"
#include "checksum.h"
#include "timer.h"

#define MAX_BYTES                     32
#define I2C_DEV_ADDRESS               0xA0
#define I2C_TIMEOUT_MS                1500 //1000 ms
#define I2C_EEPROM_SIZE               0x8000
#define WRITE_INDEX_INFO_MAGIC_NUMBER 0xFA
#define WRITE_INDEX_INFO_SIZE         3
#define WRITE_INDEX_INFO_LOCATION     (I2C_EEPROM_SIZE - WRITE_INDEX_INFO_SIZE - 1)
#define INDEX                         0
#define WRITE_CYCLE_DELAY_MS          5
#define CHECKSUM_LENGTH               2

static bool __initialized;
static uint8_t __dev_addr;
static uint8_t __read_buffer[MAX_BYTES];
static uint8_t __write_index_info[WRITE_INDEX_INFO_SIZE];
static uint8_t __write_index;

static bool __write(uint16_t mem_addr,uint8_t *data,uint8_t size);
static bool __read(uint16_t mem_addr,uint8_t *data,uint8_t size);
static void __update_write_index(void);

/////////////////////////////////////////public functions////////////////

void eeprom_module_init(void)
{
	bool result;

	if(__initialized == true)
		return;

	__dev_addr = I2C_DEV_ADDRESS;
	result = __read(WRITE_INDEX_INFO_LOCATION,__write_index_info,WRITE_INDEX_INFO_SIZE);
	__write_index = __write_index_info[INDEX+1];

	if(result == false){
		dbgprintf("failed to read write index info\r\n");
		dbgprintf("resetting write index to 0\r\n");
		__write_index = 0;
	}
	else {
		if(__write_index_info[INDEX] != WRITE_INDEX_INFO_MAGIC_NUMBER){
			dbgprintf("magic number is mismatched\r\n");
			dbgprintf("expected %02X but read %02X\r\n",WRITE_INDEX_INFO_MAGIC_NUMBER,__write_index_info[INDEX]);
			dbgprintf("resetting write index to 0\r\n");
			__write_index = 0;
		}
		else if(__write_index_info[INDEX+2] != checksum(__write_index_info, CHECKSUM_LENGTH)) {
			dbgprintf("checksum is mismatched\r\n");
			dbgprintf("expected %02X but read %02X\r\n",checksum(__write_index_info, CHECKSUM_LENGTH),__write_index_info[INDEX+2]);
			dbgprintf("resetting write index to 0\r\n");
			__write_index = 0;
		}
	}
	__initialized = true;

	pin_set(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);

}

void eeprom_dump_data_on_uart(void)
{
	if(__initialized == false)
			return;

//	HAL_StatusTypeDef return_val;
//
//	return_val = HAL_I2C_Mem_Read(&hi2c1,__dev_addr, 0x0, I2C_MEMADD_SIZE_16BIT, __read_buffer,MAX_BYTES,I2C_TIMEOUT_MS);

	bool result;
	result = __read(0x00, __read_buffer, MAX_BYTES);
	if(result == false){
		dbgprintf("failed to read eeprom(dev address %02X)\r\n",__dev_addr);
		return;
	}

	result = __read(WRITE_INDEX_INFO_LOCATION, __write_index_info, WRITE_INDEX_INFO_SIZE);
	if(result == false){
		dbgprintf("failed to read eeprom(dev address %02X)\r\n",__dev_addr);
		return;
	}

	for(uint16_t i=0; i < MAX_BYTES; i++) {
		if(isprint(__read_buffer[i]) != 0)
			dbgprintf(" %c",__read_buffer[i]);
		else
			dbgprintf(" %02X", __read_buffer[i]);
	}

	dbgprintf("[%02X %02X %02X]\r\n",__write_index_info[INDEX],__write_index_info[INDEX+1],__write_index_info[INDEX+2]);

}

bool eeprom_write(uint8_t data_byte)
{
	if(__initialized == false)
			return false;

	bool result;
	result = __write(__write_index, &data_byte, 1);

//	HAL_StatusTypeDef return_value;
//
//	pin_clear(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
//	return_value = HAL_I2C_Mem_Write(&hi2c1, __dev_addr, __write_index, I2C_MEMADD_SIZE_16BIT, &data_byte,1,I2C_TIMEOUT_MS);
//	pin_set(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);

	if(result == false){
		dbgprintf("failed to write eeprom(dev address %02X) at location %d\r\n",__dev_addr,__write_index);
		return false;
	}

	__write_index = ((__write_index+1) % MAX_BYTES);
	__update_write_index();
	return true;

}

/////////////////////////private functions/////////////////////

static bool __write(uint16_t mem_addr,uint8_t *data,uint8_t size)
{
	if(!data)
		return false;

	HAL_StatusTypeDef return_value;

	pin_clear(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
	return_value = HAL_I2C_Mem_Write(&hi2c1, __dev_addr, mem_addr, I2C_MEMADD_SIZE_16BIT, data,size,I2C_TIMEOUT_MS);
	pin_set(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);

	timer_delay_ms(WRITE_CYCLE_DELAY_MS);
	if(return_value != HAL_OK)
		return false;

	return true;

}

static bool __read(uint16_t mem_addr,uint8_t *data,uint8_t size)
{
	if(!data)
		return false;

	HAL_StatusTypeDef return_val;

	return_val = HAL_I2C_Mem_Read(&hi2c1,__dev_addr, mem_addr, I2C_MEMADD_SIZE_16BIT, data,size,I2C_TIMEOUT_MS);
	if(return_val != HAL_OK)
		return false;

	return true;
}

static void __update_write_index(void)
{
	bool result;
	__write_index_info[INDEX]   = WRITE_INDEX_INFO_MAGIC_NUMBER;
	__write_index_info[INDEX+1] = __write_index;
	__write_index_info[INDEX+2] = checksum(__write_index_info, CHECKSUM_LENGTH);

	result = __write(WRITE_INDEX_INFO_LOCATION, __write_index_info, WRITE_INDEX_INFO_SIZE);

	if(result == false)
		dbgprintf("failed to update write index info\r\n");

}
